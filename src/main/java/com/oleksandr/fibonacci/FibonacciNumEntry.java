package com.oleksandr.fibonacci;

import java.util.Scanner;

/** Class FibonacciNumEntry shows
 * fibonacci numbers from the interval
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class FibonacciNumEntry extends MenuEntry {
    /** {@link FibonacciNumerator} instance */
    private FibonacciNumerator fibonacciNumerator;

    /** {@link OddEvenIdentifier} instance */
    private OddEvenIdentifier oddEven;

    /** Constructor, which sets an explanation of
     * this menu entry
     * to show Fibonacci Numbers from the interval*/
    public FibonacciNumEntry() {
        this.setExplanation("Show Fibonacci Numbers from the interval");

        fibonacciNumerator = new FibonacciNumerator();
        oddEven = new OddEvenIdentifier();
    }

    /** run() method gets called from the menu and does
     * what is described in an explanation:
     * shows fibonacci numbers from the interval
     * @param leftBound - left interval bound
     * @param rightBound - right interval bound */
    public final void run(final int leftBound, final int rightBound) {
        Scanner scanner = new Scanner(System.in, "UTF-8");

        System.out.println("Please, enter amount of numbers,"
                + " which you'd like to include:");

        while (!(scanner.hasNextInt())) {
            scanner.next();
            System.out.print("Please enter an integer: ");
        }
        int fibonacciNumbersAmount = scanner.nextInt();

        int firstNumber = oddEven.getMaxOddNumber(leftBound, rightBound);
        int secondNumber = oddEven.getMaxEvenNumber(leftBound, rightBound);

        fibonacciNumerator.showFibonacciNumbers(
                fibonacciNumbersAmount, firstNumber, secondNumber);
    }
}
