package com.oleksandr.fibonacci;

/** Class OddAndEvenSumEntry shows
 * sum of odd and even numbers
 * from the interval
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class OddAndEvenSumEntry extends MenuEntry {
    /** {@link OddEvenIdentifier} instance */
    private OddEvenIdentifier oddEven;

    /** Constructor, which sets an explanation of
     * this menu entry
     * to show  odd and even numbers sum*/
    public OddAndEvenSumEntry() {
        oddEven = new OddEvenIdentifier();
        this.setExplanation("Show odd and even numbers sum");
    }

    /** run() method gets called from the menu and does
     * what is described in an explanation:
     * shows sum of odd and even numbers from the interval
     * @param leftBound - left interval bound
     * @param rightBound - right interval bound */
    public final void run(
            final int leftBound,
             final int rightBound) {
        int[] odd = oddEven.calculateOdd(leftBound, rightBound);
        int[] even = oddEven.calculateEven(leftBound, rightBound);

        System.out.print("Odd numbers sum: ");
        showArraySum(odd);
        System.out.print("\n Even numbers sum: ");
        showArraySum(even);
    }

    /** Method to show sum of elements of given array
     * @param array - array, sum of elements of which will be calculated*/
    private void showArraySum(final int[] array) {
        int sum = 0;

        for (int num : array) {
            sum += num;
        }

        System.out.print(sum);
    }
}
