package com.oleksandr.fibonacci;

import java.util.Scanner;

/** Class FibonacciPercentageEntry shows
 * percentage of odd and even numbers
 * in Fibonacci sequence
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class FibonacciPercentageEntry extends MenuEntry {
    /** {@link FibonacciNumerator} instance */
    private FibonacciNumerator fibonacciNumerator;

    /** {@link OddEvenIdentifier} instance */
    private OddEvenIdentifier oddEven;

    /** Constructor, which sets an explanation of
     * this menu entry
     * to show percentage of odd and even Fibonacci numbers*/
    FibonacciPercentageEntry() {
        this.setExplanation(
                "Show percentage of odd and even fibonacci numbers");

        fibonacciNumerator = new FibonacciNumerator();
        oddEven = new OddEvenIdentifier();
    }

    /** run() method gets called from the menu and does
     * what is described in an explanation:
     * shows percentage of odd and even Fibonacci numbers
     * @param leftBound - left interval bound
     * @param rightBound - right interval bound */
    public final void run(
            final int leftBound,
            final int rightBound) {
        Scanner scanner = new Scanner(System.in, "UTF-8");

        System.out.println("Please, enter amount of numbers,"
                + " which you'd like to include:");

        while (!(scanner.hasNextInt())) {
            scanner.next();
            System.out.print("Please enter an integer: ");
        }
        int fibonacciNumbersAmount = scanner.nextInt();

        int firstNumber = oddEven.getMaxOddNumber(
                leftBound, rightBound);
        int secondNumber = oddEven.getMaxEvenNumber(
                leftBound, rightBound);

        fibonacciNumerator.showOddAndEvenPercentage(
                fibonacciNumbersAmount, firstNumber, secondNumber);
    }
}
