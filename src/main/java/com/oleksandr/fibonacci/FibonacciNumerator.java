package com.oleksandr.fibonacci;

/** /** Class FibonacciNumerator is used
 * to numerate Fibonacci numbers
 * and deal with them
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class FibonacciNumerator {
    /**{@link OddEvenIdentifier} instance */
    private OddEvenIdentifier oddEven;

    /** Constructor of FibonacciNumerator */
    FibonacciNumerator() {
        oddEven = new OddEvenIdentifier();
    }

    /** Method to show Fibonacci numbers
     * @param fibonacciNumbersAmount - desired amount of Fibonacci numbers
     * @param firstNumber - first member of Fibonacci sequence
     * @param secondNumber - second member of Fibonacci */
    public final void showFibonacciNumbers(
             final int fibonacciNumbersAmount,
             final int firstNumber,
             final int secondNumber) {

        int[] fibonacciNumbers = calculateFibonacciNumbers(
                fibonacciNumbersAmount, firstNumber, secondNumber);

        for (int num : fibonacciNumbers) {
            System.out.print(num + " ");
        }
        System.out.println();
    }

    /** Method to show percentage of odd and even
     * numbers in particular fibonacci sequence
     * @param fibonacciNumbersAmount - desired amount of Fibonacci numbers
     * @param firstNumber - first member of Fibonacci sequence
     * @param secondNumber - second member of Fibonacci */
    public final void showOddAndEvenPercentage(
            final int fibonacciNumbersAmount,
             final int firstNumber,
             final int secondNumber) {
        int[] fibonacciNumbers = calculateFibonacciNumbers(
                fibonacciNumbersAmount, firstNumber, secondNumber);

        int oddNumbersAmount = oddEven.
                getOddNumbersAmount(fibonacciNumbers);
        int evenNumbersAmount = oddEven.
                getEvenNumbersAmount(fibonacciNumbers);

        final int maxPercentage = 100;

        int oddNumbersPercentage =
                (oddNumbersAmount * maxPercentage)
                        / fibonacciNumbersAmount;
        int evenNumbersPercentage =
                (evenNumbersAmount * maxPercentage)
                        / fibonacciNumbersAmount;

        for (int num : fibonacciNumbers) {
            System.out.print(num + " ");
        }

        System.out.println("\nOdd numbers percentage : "
                + oddNumbersPercentage + "%");
        System.out.println("Even numbers percentage : "
                + evenNumbersPercentage + "%");
    }

    /** Method to calculate Fibonacci sequence
     *
     * @param fibonacciNumbersAmount - desired amount of Fibonacci numbers
     * @param firstNumber - first member of Fibonacci sequence
     * @param secondNumber - second member of Fibonacci
     * @return returns array representation of Fibonacci sequence
     */
    public final int[] calculateFibonacciNumbers(
            final int fibonacciNumbersAmount,
             final int firstNumber,
             final int secondNumber) {

        int[] fibonacciNumbers = new int[fibonacciNumbersAmount];
        fibonacciNumbers[0] = firstNumber;
        fibonacciNumbers[1] = secondNumber;

        for (int i = 2; i < fibonacciNumbersAmount; i++) {
            fibonacciNumbers[i] = fibonacciNumbers[i - 1]
                    + fibonacciNumbers[i - 2];
        }

        return fibonacciNumbers;
    }
}
