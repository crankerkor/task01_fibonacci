package com.oleksandr.fibonacci;

/** Class OddAndEvenEntry shows
 * odd and even numbers
 * from the interval
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class OddAndEvenEntry extends MenuEntry {
    /** {@link OddEvenIdentifier} instance */
    private OddEvenIdentifier oddEven;

    /** Constructor, which sets an explanation of
     * this menu entry
     * to show  odd and even numbers from the interval*/
   public OddAndEvenEntry() {
        this.setExplanation(
                "Show odd and even numbers from the interval");

        oddEven = new OddEvenIdentifier();
    }

    @Override
    /** run() method gets called from the menu and does
     * what is described in an explanation:
     * shows odd and even numbers from the interval
     * @param leftBound - left interval bound
     * @param rightBound - right interval bound */
        public final void run(
            final int leftBound,
             final int rightBound) {

           int[] odd = oddEven.calculateOdd(leftBound, rightBound);
           int[] even = oddEven.calculateEven(leftBound, rightBound);

            showOddAndEvenNumbers(odd, even);
    }

    /** Method, which prints odd and even numbers
     * @param even - array of even numbers from the interval
     * @param odd - array of odd numbers from the interval*/
    private static void showOddAndEvenNumbers(
            final int[] odd, final int[] even) {
        System.out.print("Odd numbers: ");
        for (int oddNum : odd) {
            System.out.print(oddNum + " ");
        }

        System.out.println("\nEven numbers: ");
        for (int i = even.length - 1; i >= 0; i--) {
            System.out.print(even[i] + " ");
        }
    }
}
