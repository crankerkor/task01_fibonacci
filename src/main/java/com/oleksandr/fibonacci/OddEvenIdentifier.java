package com.oleksandr.fibonacci;

/** Class OddEvenIdentifier identifies
 * operations with odd and even numbers
 * from the interval
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class OddEvenIdentifier {

    /** Method to find odd numbers from the interval
     * @param leftBound - left bound of the interval
     * @param rightBound - right bound of the interval
     * @return - returns array of odd numbers from the interval*/
    public final int[] calculateOdd(
            final int leftBound,
             final int rightBound) {
        int oddAmount = calculateOddAmount(leftBound, rightBound);

        int[] odd = new int[oddAmount];

        int oddCounter = 0;

        for (int i = leftBound; i <= rightBound; i++) {
            if (i % 2 != 0) {
                odd[oddCounter] = i;
                oddCounter++;
            }
        }

        return odd;
    }
    /** Method to find even numbers from the interval
     * @param leftBound - left bound of the interval
     * @param rightBound - right bound of the interval
     * @return - returns array of even numbers from the interval*/
    public final int[] calculateEven(
            final int leftBound,
             final int rightBound) {
        int evenAmount = calculateEvenAmount(leftBound, rightBound);

        int[] even = new int[evenAmount];

        int evenCounter = 0;

        for (int i = leftBound; i <= rightBound; i++) {
            if (i % 2 == 0) {
                even[evenCounter] = i;
                evenCounter++;
            }
        }

        return even;
    }

    /** Method to get max odd number from the interval
     * @param leftBound - left bound of the interval
     * @param rightBound - right bound of the interval
     * @return - returns max odd number from the interval*/
    public final int getMaxOddNumber(
            final int leftBound,
             final int rightBound) {
        int[] odd = calculateOdd(leftBound, rightBound);

        return odd[odd.length - 1];
    }

    /** Method to get max even number from the interval
     * @param leftBound - left bound of the interval
     * @param rightBound - right bound of the interval
     * @return - returns max even number from the interval*/
    public final int getMaxEvenNumber(
            final int leftBound,
             final int rightBound) {
        int[] even = calculateEven(leftBound, rightBound);

        return even[even.length - 1];
    }

    /** Method to get amount of odd number in the array
     * @return - returns the amount of odd number in the array */
    public final int getOddNumbersAmount(
            final int[] array) {
        int oddNumbersAmount = 0;

        for (int number : array) {
            if (number % 2 != 0) {
                oddNumbersAmount++;
            }
        }

        return oddNumbersAmount;
    }

    /** Method to get amount of even number in the array
     * @return - returns the amount of even number in the array */
    public final int getEvenNumbersAmount(
            final int[] array) {
        int evenNumbersAmount = 0;

        for (int number : array) {
            if (number % 2 == 0) {
                evenNumbersAmount++;
            }
        }

        return evenNumbersAmount;
    }

    /** Method to calculate amount of odd numbers in the interval
     * @param leftBound - left bound of the interval
     * @param rightBound - right bound of the interval
     * @return the amount of odd numbers in the interval */
   private int calculateOddAmount(
           final int leftBound,
            final int rightBound) {
        int oddAmount;

        if ((leftBound % 2 == 0) && (rightBound % 2 == 0)) {
            oddAmount = (rightBound - leftBound) / 2;
        } else  {
            oddAmount = (rightBound - leftBound) / 2 + 1;
        }

        return oddAmount;
    }

    /** Method to calculate amount of even numbers in the interval
     * @param leftBound - left bound of the interval
     * @param rightBound - right bound of the interval
     * @return the amount of even numbers in the interval */
    private int calculateEvenAmount(
            final int leftBound,
             final int rightBound) {
        int evenAmount;

        if ((leftBound % 2 == 0) && (rightBound % 2 == 0)) {
            evenAmount = (rightBound - leftBound) / 2 + 1;
        } else if ((leftBound % 2 != 0) && (rightBound % 2 != 0)) {
            evenAmount = (rightBound - leftBound) / 2;
        } else {
            evenAmount = (rightBound - leftBound) / 2 + 1;
        }

        return evenAmount;
    }
}
