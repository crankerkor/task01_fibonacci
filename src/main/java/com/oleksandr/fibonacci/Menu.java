package com.oleksandr.fibonacci;

import java.util.ArrayList;
import java.util.Scanner;

/** Class Menu describes
 * menu representation
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class Menu {
    /**List of {@link MenuEntry} to contain all menu entries */
    private ArrayList<MenuEntry> entries;

    /** Left interval bound field */
    private int leftBound;
    /** Right interval bound field */
    private int rightBound;

    /** Menu constructor
     * initializes entries and bounds
     * @param intervalLeftBound - left bound of the interval
     * @param intervalRightBound - right bound of the interval */
    public Menu(final int intervalLeftBound,
            final int intervalRightBound) {
        entries = new ArrayList<MenuEntry>();
        this.leftBound = intervalLeftBound;
        this.rightBound = intervalRightBound;
    }

    /**Method to initialize all menu entries */
    public final void initializeEntries() {
        entries.add(new ExitEntry());
        entries.add(new OddAndEvenEntry());
        entries.add(new OddAndEvenSumEntry());
        entries.add(new FibonacciNumEntry());
        entries.add(new FibonacciPercentageEntry());
    }

    /**Method, which shows menu to user */
    public final void printMenu() {
        MenuEntry chosenOption;
        int chosenOptionNumber;

        do {
            System.out.println("\nChoose, what you want to do:");

            for (int i = 0; i < entries.size(); i++) {
                System.out.println(i + ") " + entries.get(i).getExplanation());
            }

            chosenOptionNumber = chooseOption(entries.size());

            chosenOption = entries.get(chosenOptionNumber);
            chosenOption.run(leftBound, rightBound);

        } while (!(chosenOption instanceof ExitEntry));
    }

    /** Method to choose option from the menu
     * @param size - amount of entries in the menu
     * @return - returns number of chosen entry*/
    private static int chooseOption(final int size) {
        int chosenOptionNumber;
        Scanner scanner = new Scanner(System.in, "UTF-8");

        do {
            System.out.print("Please enter a valid option: ");

            while (!scanner.hasNextInt()) {
                scanner.next();
                System.out.print("Please enter a valid option: ");
            }

            chosenOptionNumber = scanner.nextInt();
        } while (chosenOptionNumber >= size);

        return chosenOptionNumber;
    }
}
