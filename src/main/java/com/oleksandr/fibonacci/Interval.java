package com.oleksandr.fibonacci;

import java.util.Scanner;

/** Class Interval describes
 * an integer interval
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class Interval {
    /** Left interval bound field */
    private int leftBound;
    /** Right interval bound field */
    private int rightBound;

    /** Method to set interval
     * Bounds are entered by user*/
    public final void setInterval() throws NumberFormatException {

        System.out.println("Set the interval, please");
        System.out.print("From: ");

        leftBound = readInt();

        do {
          System.out.print("To (bigger than previous one): ");

          rightBound = readInt();
        } while (rightBound <= leftBound);
    }

    private int readInt() {
        Scanner scanner = new Scanner(System.in, "UTF-8");
        int num = 0;
        try {
          num = Integer.parseInt(scanner.next());

        } catch(NumberFormatException e) {
            System.out.print("Please enter an integer: ");
            readInt();
        }

        return num;
    }

    /** Method to get left bound
     * @return returns left bound of the interval*/
    public final int getLeftBound() {
        return leftBound;
    }

    /** Method to get right bound
     * @return returns right bound of the interval*/
    public final int getRightBound() {
        return rightBound;
    }
}
