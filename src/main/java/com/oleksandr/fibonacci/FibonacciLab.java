package com.oleksandr.fibonacci;
/**
 * Main Class for Fibonacci_Lab01.
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
final class FibonacciLab {
    /** Main method */
    public static void main(final String[] args) {
        Interval interval = new Interval();

        try {
        interval.setInterval();
        } catch (NumberFormatException e) {
            System.out.print("Interval was not set due to" +
                    "wrong input. Try again later");
        }
        int leftBound = interval.getLeftBound();
        int rightBound = interval.getRightBound();

        Menu menu = new Menu(leftBound, rightBound);
        menu.initializeEntries();
        menu.printMenu();
    }
}
