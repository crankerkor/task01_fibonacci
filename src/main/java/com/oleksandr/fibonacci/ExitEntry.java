package com.oleksandr.fibonacci;

/** Class to allow exit from the menu
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public class ExitEntry extends MenuEntry {
    /** Constructor, which sets an explanation, showed in the menu
     *
     */
    ExitEntry() {
        this.setExplanation("Exit the program");
    }

    /** run() method just prints success message
     *
     */
    public final void run(final int left, final int right) {
        System.out.println("Successfully exited");
    }

}
