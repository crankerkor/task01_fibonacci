package com.oleksandr.fibonacci;

/** Abstract class MenuEntry describes
 * menu entry
 * @author Oleksandr Lutsenko
 * @version 1.0
 */
public abstract class MenuEntry {
    /**String field of menu entry explanation */
    private String explanation;

    /**Abstract run method, which will be overloaded
     * in real menu entries
     * @param left - left bound of the interval
     * @param right - right bound of the interval*/
    abstract void run(int left, int right);

    /** Method to get explanation of menu entry
     * @return returns String explanation */
    final String getExplanation() {
        return explanation;
    }

    /** Method to set explanation of menu entry
     * @param exp - explanation to be set*/
    final void setExplanation(final String exp) {
        this.explanation = exp;
    }
}
